"""
Template - Create a list formed by 8 copies of True and 8 copies of False
"""

# Uncomment and enter code here

truefalse_list = 8 * [True] + 8 * [False]
print(truefalse_list)


# Output
#[True, True, True, True, True, True, True, True, False, False, False, False, False, False, False, False]
