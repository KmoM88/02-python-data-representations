"""
Project for Week 4 of "Python Data Representations".
Find differences in file contents.

Be sure to read the project description page for further information
about the expected behavior of the program.
"""

IDENTICAL = -1


def singleline_diff(line1, line2):
    """
    Inputs:
      line1 - first single line string
      line2 - second single line string
    Output:
      Returns the index where the first difference between
      line1 and line2 occurs.

      Returns IDENTICAL if the two lines are the same.
    """
    if len(line1) != len(line2):
        if len(line1) < len(line2):
            for element in range(len(line1)):
                if line1[element] != line2[element]:
                    return element
            return len(line1)
        else:
            for element in range(len(line2)):
                if line1[element] != line2[element]:
                    return element
            return len(line2)
    else:
        for element in range(len(line1)):
            if line1[element] != line2[element]:
                return element
        return IDENTICAL

# print(singleline_diff('Python i  fun!','Python is fun!!!'))


def singleline_diff_format(line1, line2, idx):
    """
    Inputs:
      line1 - first single line string
      line2 - second single line string
      idx   - index at which to indicate difference
    Output:
      Returns a three line formatted string showing the location
      of the first difference between line1 and line2.

      If either input line contains a newline or carriage return,
      then returns an empty string.

      If idx is not a valid index, then returns an empty string.
    """
    len_line1 = len(line1)
    len_line2 = len(line2)
    line_diff = singleline_diff(line1, line2)
    # print(len_line1)
    # print(len_line2)
    # print(line_diff)
    if (line1.find("\n") != -1) or (line2.find("\n") != -1):
        return ""
    if idx == -1:
        return ""
    if ((idx > len_line1) or (idx > len_line2)):
        return ""
    else:
        separator_str = ""
        for i in range(idx):
            separator_str += "="
        separator_str += "^"
        return line1 + "\n" + separator_str + "\n" + line2 + "\n"

# print(singleline_diff_format('abcdefg', 'abc', 5))

def multiline_diff(lines1, lines2):
    """
    Inputs:
      lines1 - list of single line strings
      lines2 - list of single line strings
    Output:
      Returns a tuple containing the line number (starting from 0) and
      the index in that line where the first difference between lines1
      and lines2 occurs.

      Returns (IDENTICAL, IDENTICAL) if the two lists are the same.
    """
    # print(lines1 + lines2)
    len_lines1 = len(lines1)
    len_lines2 = len(lines2)
    diffs = []
    # print(len_lines1)
    # print(len_lines2)
    # print(lines1[0])
    # print(len(lines2))
    if len_lines1 == len_lines2:
        for line in range(len_lines1):
            # print(lines1[line])
            diffs.append((line, singleline_diff(lines1[line], lines2[line])))
        # print(diffs)
        test_identical = True
        number_diffs = 0
        for diff in range(len(diffs)):
            # print(diff)
            if diffs[diff][1] != -1:
                test_identical = False
                number_diffs += 1
        if test_identical:
            return (IDENTICAL, IDENTICAL)
        else:
            # print(number_diffs)
            # print(test_identical)
            for diff in range(len(diffs)):
                if diffs[diff][1] != -1:
                    return diffs[diff]
    else:
        test_identical = True
        number_diffs = 0
        if len_lines1 < len_lines2:
            for line in range(len_lines1):
                # print(lines1[line])
                diffs.append((line, singleline_diff(lines1[line], lines2[line])))
            # print(diffs)
            test_identical = True
            number_diffs = 0
            for diff in range(len(diffs)):
                # print(diff)
                if diffs[diff][1] != -1:
                    test_identical = False
                    number_diffs += 1
            if test_identical:
                return (len_lines1, 0)
            else:
                # print(number_diffs)
                # print(test_identical)
                for diff in range(len(diffs)):
                    if diffs[diff][1] != -1:
                        return diffs[diff]
        else:
            for line in range(len_lines2):
                # print(lines1[line])
                diffs.append((line, singleline_diff(lines1[line], lines2[line])))
            # print(diffs)
            test_identical = True
            number_diffs = 0
            for diff in range(len(diffs)):
                # print(diff)
                if diffs[diff][1] != -1:
                    test_identical = False
                    number_diffs += 1
            if test_identical:
                return (len_lines2, 0)
            else:
                # print(number_diffs)
                # print(test_identical)
                for diff in range(len(diffs)):
                    if diffs[diff][1] != -1:
                        return diffs[diff]


# print(multiline_diff(['line1', 'line2'], ['line1', 'line2', 'line3']))


def get_file_lines(filename):
    """
    Inputs:
      filename - name of file to read
    Output:
      Returns a list of lines from the file named filename.  Each
      line will be a single line string with no newline ('\n') or
      return ('\r') characters.

      If the file does not exist or is not readable, then the
      behavior of this function is undefined.
    """
    datafile1 = open(filename, "rt")
    lines = []
    for line in datafile1:
      if line[-1] == "\n":
          lines.append(line[:-1])
      else:
          lines.append(line)

    datafile1.close()
    return lines

# print(get_file_lines('file1.txt'))


def file_diff_format(filename1, filename2):
    """
    Inputs:
      filename1 - name of first file
      filename2 - name of second file
    Output:
      Returns a four line string showing the location of the first
      difference between the two files named by the inputs.

      If the files are identical, the function instead returns the
      string "No differences\n".

      If either file does not exist or is not readable, then the
      behavior of this function is undefined.
    """
    file1_lines = get_file_lines(filename1)
    file2_lines = get_file_lines(filename2)
    multi_diff = multiline_diff(file1_lines, file2_lines)
    # print(multi_diff)
    # print(file2_lines)
    if multi_diff == (-1, -1):
        return 'No differences\n'
    if multi_diff[1] != 0:
        str_return = "Line " + str(multi_diff[0]) + ":\n" + file1_lines[multi_diff[0]] + "\n"
        for i in range(multi_diff[1]):
            str_return += "="
        str_return += ("^" + "\n" + str(file2_lines[multi_diff[0]]) + "\n")
    else:
        if ((len(file1_lines) != 0) and (len(file2_lines) != 0)):
            str_return = "Line " + str(multi_diff[0]) + ":\n" + file1_lines[0] + "\n"
            str_return += ("^" + "\n" + file2_lines[0] + "\n")
        elif len(file1_lines) == 0:
            str_return = "Line " + str(multi_diff[0]) + ":\n" + "\n"
            str_return += ("^" + "\n" + file2_lines[0] + "\n")
        else:
            str_return = "Line " + str(multi_diff[0]) + ":\n" + file1_lines[0] + "\n"
            str_return += ("^" + "\n" +  "\n")

    return str_return

# print(file_diff_format('file1.txt', 'file2.txt'))