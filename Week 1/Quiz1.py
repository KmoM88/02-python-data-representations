def count_vowels(word):
    """docstring"""
    vowels = ["a", "e", "i", "o", "u"]
    sum = 0
    for x in vowels:
        sum = sum + word.count(x)
    return sum

def demistify(li_string):
    """docstring"""
    return_string = ""
    for i in range(len(li_string)):
        # print(li_string[i])
        # if li_string[i] == "1":
        #     print(i)
        #     return_string = return_string + "b"
        if li_string[i] == "l":
            return_string == return_string + "a"
    return return_string

print(count_vowels("aovvouOucvicIIOveeOIclOeuvvauouuvciOIsle"))
print(demistify("lll111l1l1l1111lll"))