"""
Template - Function that swaps and capitalizes first and last names
"""


def name_swap(name_string):
    """
    Given the string name string of the form "first last", return
    the string "Last First" where both names are now capitalized
    """

    # Enter code here
    space_index = name_string.find(" ")
    if space_index != -1:
        return name_string[space_index + 1:].upper() + " " + name_string[0: space_index]
    else:
        return "Error"
# Tests

print(name_swap("joe warren"))
print(name_swap("scott rixner"))
print(name_swap("john greiner"))


# Output

#Warren Joe
#Rixner Scott
#Greiner John