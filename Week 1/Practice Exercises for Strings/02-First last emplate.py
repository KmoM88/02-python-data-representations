"""
Template - Item selection for lists
"""

# Create a string formed by selecting the first and last letters of example_string
example_string = "It's just a flesh wound"
print(example_string)

fisrt_string = example_string[0]
last_string = example_string[-1]
solution_string = fisrt_string + last_string
print(solution_string)

# Output should be 
#It's just a flesh wound
#Id