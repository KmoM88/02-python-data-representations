# instructors = ("Scott", "Joe", "John", "Stephen")
# instructors[2 : 4] = []
# print(instructors)

# my_list = [1, 3, 5, 7, 9]
# my_list.reverse()
# print(my_list.reverse())

# fib = [0, 1]

# for i in range(10):
#     fib.append(fib[-2]+fib[-1])

# print(fib)

"""
Implement the Sieve of Eratosthenes
https://en.wikipedia.org/wiki/Sieve_of_Eratosthenes
"""

def compute_primes(bound):
    """
    Return a list of the prime numbers in range(2, bound)
    """

    answer = list(range(2, bound))
    # print(answer)
    for divisor in range(2, bound):
        # Remove appropriate multiples of divisor from answer
        for x in range(divisor, bound, divisor):
            if x in answer and x != divisor:
                # print("Removing " + str(x))
                answer.remove(x)
    return answer

print(len(compute_primes(200)))
print(len(compute_primes(2000)))